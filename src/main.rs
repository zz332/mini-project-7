use std::env;
use std::fs::File;
use std::io::Read;
use serde::Deserialize;
use csv::ReaderBuilder;

#[derive(Debug, Deserialize, Clone)]
struct Car {
    Car_Name: String,
    Year: u32,
    Selling_Price: f64,
    Present_Price: f64,
    Kms_Driven: u32,
    Fuel_Type: String,
    Seller_Type: String,
    Transmission: String,
    Owner: u8,
}

fn read_csv(file_path: &str) -> Vec<Car> {
    let mut rdr = ReaderBuilder::new().from_path(file_path).unwrap();
    rdr.deserialize().map(|result| result.unwrap()).collect()
}

fn filter_and_sort_cars_by_fuel_type(cars: &[Car], fuel_type: &str) -> Vec<Car> {
    let mut filtered: Vec<Car> = cars.iter()
                                     .filter(|car| car.Fuel_Type.eq_ignore_ascii_case(fuel_type))
                                     .cloned()
                                     .collect();
    filtered.sort_by(|a, b| b.Selling_Price.partial_cmp(&a.Selling_Price).unwrap());
    filtered
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = "cardata.csv"; // Adjust the file path as necessary
    let cars = read_csv(file_path);

    if args.len() > 1 {
        let fuel_type = &args[1];
        let filtered_cars = filter_and_sort_cars_by_fuel_type(&cars, fuel_type);
        println!("Top 10 {} cars sorted by selling price:", fuel_type);
        for car in filtered_cars.iter().take(10) {
            println!("\"{}\" ({}) - ${:.2}", car.Car_Name, car.Year, car.Selling_Price);
        }
    } else {
        if let Some(first_car) = cars.first() {
            println!("First car: \"{}\" ({})", first_car.Car_Name, first_car.Year);
        }
        println!("Total number of cars: {}", cars.len());
    }
}

