# Data Processing with Vector Database

## Description

This project performs data processing and analysis on a dataset of cars. The application reads a CSV file containing car information, including car names, years, selling prices, present prices, kilometers driven, fuel types, seller types, transmission types, and ownership details. It efficiently filters and aggregates this data based on user-specified criteria, such as fuel type. With functionalities to display sorted car data based on selling prices, the project demonstrates an approach to data manipulation and querying within a vector-based storage model.

## Demo 

- **Default Test**

When using `cargo run`, the default operations will be invoked and return general information about the full dataset, including the first presented car's name and year, and the total number of cars in the dataset:

![default](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG84.jpg)

- **Test with Specified Argument**

The functions also allow users to specify detailed arguments upon calling, which will yield filtered output.

When using `cargo run Petrol`, the functions will return the top 10 cars belonging to the `Petrol` fuel type, ordered by selling price:

![Petrol](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG85.jpg)

When using `cargo run Diesel`, the functions will return the top 10 cars belonging to the `Diesel` fuel type, ordered by selling price:

![Diesel](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG86.jpg)

You can always specify other types of fuel types for customized results.


## Steps Walkthrough

- Run `cargo new miniproject7`
- Add `serde`, `serde_json`, and `csv` to `Cargo.toml`
- Build Rust functions in `src/main.rs`
- Compile it by running `cargo build`
- `cargo run` or with specific arguments 

## Detailed Structure

- **Car Structure**

Define the structure of each car instance based on available variables in our CSV file:
```rust
struct Car {
    Car_Name: String,
    Year: u32,
    Selling_Price: f64,
    Present_Price: f64,
    Kms_Driven: u32,
    Fuel_Type: String,
    Seller_Type: String,
    Transmission: String,
    Owner: u8,
}
```
Each row in the file will then be read as individual `Car` instance following the structure. 

- **Filtering & Aggregation**

Filter and aggregate functions are defined to select books from specific genres and calculate gross book sales:
```rust
fn filter_and_sort_cars_by_fuel_type(cars: &[Car], fuel_type: &str) -> Vec<Car> {
    let mut filtered: Vec<Car> = cars.iter()
                                     .filter(|car| car.Fuel_Type.eq_ignore_ascii_case(fuel_type))
                                     .cloned()
                                     .collect();
    filtered.sort_by(|a, b| b.Selling_Price.partial_cmp(&a.Selling_Price).unwrap());
    filtered
}
```

- **Output Conditions**

Inside the `main()` function, we detail the conditions of `cargo run`, where two different sets of output are considered based on the length of the input arguments: 

```rust
if args.len() > 1 {
        let fuel_type = &args[1];
        let filtered_cars = filter_and_sort_cars_by_fuel_type(&cars, fuel_type);
        println!("Top 10 {} cars sorted by selling price:", fuel_type);
        for car in filtered_cars.iter().take(10) {
            println!("\"{}\" ({}) - ${:.2}", car.Car_Name, car.Year, car.Selling_Price);
        }
    } else {
        if let Some(first_car) = cars.first() {
            println!("First car: \"{}\" ({})", first_car.Car_Name, first_car.Year);
        }
        println!("Total number of cars: {}", cars.len());
    }
```
